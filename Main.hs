module Main where

import Prelude hiding ( null )

import Control.Arrow
import Data.Profunctor.Product.TH
import Data.Text ( Text )
import Data.Time
import Data.UUID ( UUID )
import Database.PostgreSQL.Simple
import Opaleye
import System.Environment

import qualified Data.ByteString.Char8 as BC

-- newtype UserId = UserId PGUuid

-- deriving instance QueryRunnerColumnDefault UserId UUID

data User' uid name phone pass ctime utime =
    User
    { userId                :: uid
    , userName              :: name
    , userPhone             :: phone
    , userPasswordEncrypted :: pass
    , userCreated           :: ctime
    , userUpdated           :: utime
    } deriving (Ord, Eq, Show)

type User = User'
            UUID
            Text
            (Maybe Text)
            Text
            (Maybe UTCTime)
            (Maybe UTCTime)
type UserCol = User'
               (Column PGUuid)
               (Column PGText)
               (Column (Nullable PGText))
               (Column PGText)
               (Column PGTimestamptz)
               (Column PGTimestamptz)
type UserWCol = User'
                (Maybe (Column PGUuid))
                (Column PGText)
                (Column (Nullable PGText))
                (Column PGText)
                (Maybe (Column PGTimestamptz))
                (Maybe (Column PGTimestamptz))


$(makeAdaptorAndInstance "pUser" ''User')

usersTable :: Table UserWCol UserCol
usersTable = Table "users"
             $ pUser
               User
               { userId                = optional "id"
               , userName              = required "name"
               , userPhone             = required "phone"
               , userPasswordEncrypted = required "password_encrypted"
               , userCreated           = optional "created"
               , userUpdated           = optional "updated"
               }

data Email' eid uid email code conf =
    Email
    { emailId        :: eid
    , emailUserId    :: uid
    , emailEmail     :: email
    , emailCode      :: code
    , emailConfirmed :: conf
    } deriving (Ord, Eq, Show)

type Email = Email' UUID UUID Text (Maybe Text) (Maybe UTCTime)
type EmailCol = Email'
                (Column PGUuid)
                (Column PGUuid)
                (Column PGText)
                (Column (Nullable PGText))
                (Column (Nullable PGTimestamptz))
type EmailWCol = Email'
                (Maybe (Column PGUuid))
                (Column PGUuid)
                (Column PGText)
                (Column (Nullable PGText))
                (Column (Nullable PGTimestamptz))


$(makeAdaptorAndInstance "pEmail" ''Email')

emailsTable :: Table EmailWCol EmailCol
emailsTable = Table "emails"
              $ pEmail
                Email
                { emailId        = optional "id"
                , emailUserId    = required "user_id"
                , emailEmail     = required "email"
                , emailCode      = required "code"
                , emailConfirmed = required "confirmed"
                }

userEmails :: QueryArr (Column PGUuid) (Column PGText)
userEmails = proc uid -> do
    user <- queryTable usersTable -< ()
    email <- queryTable emailsTable -< ()
    restrict -< (userId user) .== (emailUserId email)
           .&& (userId user) .== uid
    returnA -< emailEmail email

main :: IO ()
main = do
    [constr] <- getArgs
    con <- connectPostgreSQL $ BC.pack constr
    let u = User
            { userId                = Nothing
            , userName              = pgStrictText "Hello"
            , userPhone             = null
            , userPasswordEncrypted = pgStrictText "pass"
            , userCreated           = Nothing
            , userUpdated           = Nothing
            }
    [uid] <- runInsertReturning con usersTable u userId

    let e = Email
            { emailId        = Nothing
            , emailUserId    = pgUUID uid
            , emailEmail     = pgStrictText "email@domain.com"
            , emailCode      = null
            , emailConfirmed = null
            }
    runInsert con emailsTable e
    let q = distinct $ (arr $ const $ pgUUID uid) >>> userEmails
    putStrLn $ showSqlForPostgres q
    x <- runQuery con q
    putStrLn $ show (x :: [Text])

    return ()
